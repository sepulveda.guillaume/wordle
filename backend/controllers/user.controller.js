const User = require("../models/User");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const register = async (req, res) => {
  try {
    const { email, password } = req.body;

    const existingUser = await User.findOne({ email });
    if (existingUser) {
      return res.status(400).json({ message: "Email address already exists" });
    }

    const hashedPassword = await bcrypt.hash(password, 10);
    const user = new User({
      email,
      password: hashedPassword,
    });
    await user.save();
    res.status(201).json({ message: "User registered successfully" });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const login = async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await User.findOne({ email });
    if (!user) {
      return res
        .status(404)
        .json({ message: "Email and/or password incorrect(s)" });
    }
    const isPasswordCorrect = await bcrypt.compare(password, user.password);
    if (!isPasswordCorrect) {
      return res
        .status(400)
        .json({ message: "Email and/or password incorrect(s)" });
    }
    const token = jwt.sign({ userId: user._id }, process.env.API_SECRET_TOKEN, {
      expiresIn: "1h",
    });
    res.status(200).json({ token, userId: user._id });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const loginGoogle = async (req, res) => {
  try {
    const { email } = req.body;
    let user = await User.findOne({ email });
    if (!user) {
      const newUser = new User({ email });
      user = await newUser.save();
    }
    user = await User.findOne({ email });
    const token = jwt.sign({ userId: user._id }, process.env.API_SECRET_TOKEN, {
      expiresIn: "1h",
    });
    res.status(200).json({ token, userId: user._id });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};


const getStatsUser = async (req, res) => {
  try {
    const { userId } = req.params;
    const user = await User.findById(userId);
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }
    res.status(200).json({ stats: user.stats });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const updateStatsUser = async (req, res) => {
  try {
    const { userId } = req.params;
    const user = await User.findById(userId);
    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    const { stats } = user;
    const { gamesPlayed, wordsGuessed, wordsFailed } = stats;
    const { newStats } = req.body;
    const {
      newGamesPlayed,
      newWordsGuessed,
      newWordsFailed,
      newCurrentStreak,
      newLongestStreak,
    } = newStats;
    const updatedStats = {
      gamesPlayed: gamesPlayed + newGamesPlayed,
      wordsGuessed: wordsGuessed + newWordsGuessed,
      wordsFailed: wordsFailed + newWordsFailed,
      currentStreak: newCurrentStreak,
      longestStreak: newLongestStreak,
    };
    user.stats = updatedStats;
    await user.save();
    res.status(200).json({ stats: updatedStats });
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

module.exports = {
  register,
  login,
  loginGoogle,
  getStatsUser,
  updateStatsUser,
};
