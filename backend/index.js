const express = require("express");
const app = express();
const cors = require("cors");
const port = process.env.PORT || 3000;
const router = require("./routes/index");
const mongoose = require("mongoose");

require("dotenv").config();

mongoose
  .connect(process.env.MANGOOSE_CONNECTION)
  .then(() => console.log("Connexion à MongoDB réussie !"))
  .catch(() => console.log("Connexion à MongoDB échouée !"));

app.use(cors());
app.use(express.json());

app.use("/api", router);

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});