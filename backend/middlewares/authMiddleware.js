const jwt = require("jsonwebtoken");


// Middleware to check if the user is authenticated to protect some routes (not used in this project)
const authMiddleware = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    const decodedToken = jwt.verify(token, process.env.API_SECRET_TOKEN);
    const userId = decodedToken.userId;
    req.auth = {
      userId: userId,
    };
    next();
  } catch (error) {
    res.status(401).json({
      message: "You don't have the permission to access to the movies",
    });
  }
};

module.exports = authMiddleware;
