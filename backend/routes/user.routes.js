const express = require("express");
const router = express.Router();
const {
  register,
  login,
  loginGoogle,
  getStatsUser,
  updateStatsUser,
} = require("../controllers/user.controller");

// POST login
router.post("/login", login);

// POST register
router.post("/register", register);

// POST register/login Google
router.post("/google", loginGoogle);

// GET stats
router.get("/stats/:userId", getStatsUser);

// Put update stats
router.put("/stats/:userId", updateStatsUser);

module.exports = router;
