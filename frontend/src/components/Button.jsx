export default function Button({ children, onClick, color = "var(--txt-color-dark)", backgroundColor = "transparent"}) {
  return (
    <button className="button" onClick={onClick} style={{color, backgroundColor}}>
      {children}
    </button>
  );
}
