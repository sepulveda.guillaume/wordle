import Button from "./Button";
import { useNavigate } from "react-router-dom";

export default function EndGameModal({
  isOpen,
  onReplayGame,
  isWin,
  wordToGuess,
}) {
  const navigate = useNavigate();

  return isOpen ? (
    <div className="display__scores__container">
      <div className="display__scores__content">
        {isWin ? (
          <>
            <h2>Congratulations! You won! 🥳</h2>
            <p>Ready to chain the wins ? 🦾</p>
          </>
        ) : (
          <>
            <h2>Sorry! You lost! 😭</h2>
            <p>
              The correct answer was :{" "}
              <span className="gamepage__correct__answer">{wordToGuess}</span>
            </p>
          </>
        )}
        <div className="display__scores__content__divider">
          <Button
            onClick={onReplayGame}
            backgroundColor={
              isWin ? "var(--main-color)" : "var(--main-color-pink)"
            }
            color="var(--main-bg-light)"
          >
            Replay
          </Button>
          <Button
            onClick={() => navigate("/")}
            backgroundColor="var(--main-color-blue)"
            color="var(--main-bg-light)"
          >
            Quit
          </Button>
        </div>
      </div>
    </div>
  ) : null;
}
