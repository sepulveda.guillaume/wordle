export default function Footer() {
  const getDateNow = () => {
    const date = new Date();
    return date.toDateString();
  };

  return (
    <footer>
      <strong>{getDateNow()}</strong>
      <br />
      Created by Guillaume thanks Tracy Bennett
    </footer>
  );
}
