import GridRow from "./GridRow";
import KeyboardKeys from "./KeyboardKeys";

export default function GameBoard({
  grid,
  alphabet,
  onLetterClick,
  onRemoveLetter,
  checkingRow,
  disabledKeyboard,
  setDisabledKeyboard,
}) {
  return (
    <div className="gameboard__container" tabIndex={disabledKeyboard ? -1 : 0}>
      <div className="gameboard__container__grid">
        {grid.map((row, rowIndex) => (
          <GridRow
            key={rowIndex}
            row={row}
            rowIndex={rowIndex}
            checkingRow={checkingRow}
            setDisabledKeyboard={setDisabledKeyboard}
          />
        ))}
      </div>
      <KeyboardKeys
        alphabet={alphabet}
        onLetterClick={onLetterClick}
        onRemoveLetter={onRemoveLetter}
      />
    </div>
  );
}
