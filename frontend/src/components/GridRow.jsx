import { motion } from "framer-motion";
import { useEffect, useState } from "react";

export default function GridRow({
  row,
  rowIndex,
  checkingRow,
  setDisabledKeyboard,
}) {
  const [processedIndex, setProcessedIndex] = useState(0);
  const [currentColors, setCurrentColors] = useState([]);
  const [stopAnimation, setStopAnimation] = useState(false);

  useEffect(() => {
    if (checkingRow === rowIndex && !stopAnimation) {
      setDisabledKeyboard(true);
      const timeout = setTimeout(() => {
        setProcessedIndex(processedIndex + 1);
        setCurrentColors((prev) => [...prev, row[processedIndex].color]);
      }, 100);

      return () => clearTimeout(timeout);
    }
  }, [
    checkingRow,
    rowIndex,
    processedIndex,
    row,
    stopAnimation,
    setDisabledKeyboard,
  ]);

  useEffect(() => {
    if (checkingRow === rowIndex && processedIndex === row.length) {
      setStopAnimation(true);
      setDisabledKeyboard(false);
    }
  }, [checkingRow, rowIndex, processedIndex, row, setDisabledKeyboard]);

  return (
    <motion.div
      className="gameboard__container__grid__row"
      initial={{ scale: 0, opacity: 0 }}
      animate={{
        scale: 1,
        opacity: 1,
      }}
      transition={{ duration: 0.3, delay: rowIndex * 0.1 }}
      key={rowIndex}
    >
      {row.map((cell, cellIndex) => (
        <motion.div
          key={cellIndex}
          className={`gameboard__container__grid__row__cell ${
            checkingRow === rowIndex && !stopAnimation
              ? currentColors[cellIndex]
              : cell?.color
          }`}
        >
          {cell && cell.letter}
        </motion.div>
      ))}
    </motion.div>
  );
}
