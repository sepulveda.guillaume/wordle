import { Link, useNavigate } from "react-router-dom";
import { Icon } from "@iconify/react";
import {} from "react-router-dom";

export default function HeaderBar() {
  const navigate = useNavigate();

  const handleGoBack = () => {
    navigate(-1);
  };

  return (
    <header className="headerbar">
      <div className="headerbar__container">
        <div onClick={handleGoBack}>
          <Icon
            icon="formkit:arrowleft"
            width="2em"
            height="2em"
            color="var(--txt-color-dark)"
          />
        </div>
        <Link to="/">
          <img src="./wordle.svg" alt="wordle logo" height={50} width="auto" />
        </Link>
        <div className="headerbar__container__icons"></div>
      </div>
    </header>
  );
}
