export default function HeaderHome() {
  return (
    <>
      <img src="./wordle.svg" alt="wordle logo" height={80} width={90} />
      <h1>Wordle</h1>
      <p>Get 6 chances to guess a 5-letter word.</p>
    </>
  );
}
