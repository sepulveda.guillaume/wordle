import { motion } from "framer-motion";

export default function KeyboardKey({ index, letter, onLetterClick }) {
  const isMobile = window.innerWidth <= 768;
  return isMobile ? (
    <div
      className="gameboard__container__alphabet__button"
      onClick={() => onLetterClick(letter)}
    >
      {letter}
    </div>
  ) : (
    <motion.button
      disabled={true}
      className="gameboard__container__alphabet__button"
      whileTap={{ scale: 1.2 }}
      whileHover={{ scale: 1.1 }}
      onTap={() => onLetterClick(letter)}
      initial={{ scale: 0, opacity: 0 }}
      animate={{
        scale: 1,
        opacity: 1,
      }}
      transition={{ duration: 0.2, delay: index * 0.01 }}
    >
      {letter}
    </motion.button>
  );
}
