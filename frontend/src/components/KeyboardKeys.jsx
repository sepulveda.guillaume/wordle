
import { Icon } from "@iconify/react/dist/iconify.js";
import KeyboardKey from "./KeyboardKey";

export default function KeyboardKeys({alphabet, onLetterClick, onRemoveLetter}) {
  return (
    <div className="gameboard__container__alphabet">
    {alphabet.map((letter, index) => (
      <KeyboardKey key={index} index={index} letter={letter} onLetterClick={onLetterClick} />
    ))}
    <button
      className="gameboard__container__alphabet__button"
      onClick={onRemoveLetter}
    >
      <Icon icon="mynaui:delete" width="1.5em" height="1.5em" />
    </button>
  </div>
  )
}
