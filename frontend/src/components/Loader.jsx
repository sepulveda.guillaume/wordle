export default function Loader() {
  return (
    <div className="loader__circular"></div>
  )
}
