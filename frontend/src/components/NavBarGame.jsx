import { Link, useOutletContext } from "react-router-dom";
import { Icon } from "@iconify/react";

export default function NavBarGame({ setIsModalStatsOpen}) {
  const [, setIsModalRulesOpen] = useOutletContext();

  return (
    <header className="navbargame">
      <div className="navbargame__container">
        <Link to="/">
          <Icon
            icon="formkit:arrowleft"
            width="2em"
            height="2em"
            color="var(--txt-color-dark)"
          />
        </Link>
        <h2>Wordle</h2>
        <div className="navbargame__container__icons">
          <Icon
            icon="ph:question"
            width="2em"
            height="2em"
            onClick={() => setIsModalRulesOpen(true)}
            cursor="pointer"
          />

          <Icon
            icon="gridicons:stats-up-alt"
            width="2em"
            height="2em"
            onClick={() => setIsModalStatsOpen(true)}
            cursor="pointer"
          />
        </div>
      </div>
    </header>
  );
}
