import { useRef } from "react";
import Button from "./Button";
import GridRow from "./GridRow";
import useGame from "../hooks/useGame";

export default function RulesModal({ isOpen, onClose }) {
  const modalRef = useRef(null);
  const { setDisabledKeyboard } = useGame();

  const rows = [
    { letter: "H", color: "good-position" },
    { letter: "E", color: "good-position" },
    { letter: "L", color: "good-position" },
    { letter: "L", color: "good-position" },
    { letter: "O", color: "good-position" },
  ];

  const rowsWithOneGoodPosition = [
    { letter: "T", color: "wrong-letter" },
    { letter: "E", color: "good-position" },
    { letter: "S", color: "wrong-letter" },
    { letter: "T", color: "wrong-letter" },
    { letter: "S", color: "wrong-letter" },
  ];

  const rowsWithOneGoodPositionAndOneBad = [
    { letter: "W", color: "wrong-letter" },
    { letter: "O", color: "bad-position" },
    { letter: "R", color: "wrong-letter" },
    { letter: "L", color: "good-position" },
    { letter: "D", color: "wrong-letter" },
  ];

  const handleClickOutside = (event) => {
    if (modalRef.current && !modalRef.current.contains(event.target)) {
      onClose();
    }
  };

  return isOpen ? (
    <div className="modal__container" onClick={handleClickOutside}>
      <div ref={modalRef} className="modal__container__content">
        <h2>How To Play</h2>
        <h3>Guess the Wordle in 6 tries.</h3>
        <ul>
          <li>Each guess must be a valid 5-letter word.</li>
          <li>
            The color of the tiles will change to show how close your guess was
            to the word.
          </li>
        </ul>
        <p>Exemples :</p>
        <div className="modal__container__examples">
          <div className="modal__container__example">
            <p>
              One good position in{" "}
              <span style={{ color: "var(--main-color)" }}>green</span> and
              wrong letters in the world in{" "}
              <span style={{ color: "var(--main-color-pink)" }}>pink</span>
            </p>
            <GridRow
              row={rowsWithOneGoodPosition}
              setDisabledKeyboard={setDisabledKeyboard}
            />
          </div>
          <div className="modal__container__example">
            <p>
              One good position in{" "}
              <span style={{ color: "var(--main-color)" }}>green</span> and one
              in bad Position in{" "}
              <span style={{ color: "var(--main-color-yellow)" }}>yellow</span>
            </p>
            <GridRow
              row={rowsWithOneGoodPositionAndOneBad}
              setDisabledKeyboard={setDisabledKeyboard}
            />
          </div>
          <div className="modal__container__example">
            <p>
              <span style={{ color: "var(--main-color)" }}>Correct word</span>
            </p>
            <GridRow row={rows} setDisabledKeyboard={setDisabledKeyboard} />
          </div>
        </div>
        <Button
          onClick={onClose}
          backgroundColor="var(--main-color-pink)"
          color="var(--main-bg-light)"
        >
          Close
        </Button>
      </div>
    </div>
  ) : null;
}
