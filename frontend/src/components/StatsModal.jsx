import { useRef } from "react";
import Button from "./Button";
import { useNavigate } from "react-router-dom";
import Loader from "./Loader";

export default function StatsModal({ isOpen, onClose, auth, stats, loading }) {
  const navigate = useNavigate();
  const modalRef = useRef(null);

  const handleClickOutside = (event) => {
    if (modalRef.current && !modalRef.current.contains(event.target)) {
      onClose();
    }
  };

  const handleClickLogin = () => {
    onClose();
    navigate("/login");
  };

  const winRate =
    stats && stats.gamesPlayed > 0
      ? ((stats.wordsGuessed / stats.gamesPlayed) * 100).toFixed(0)
      : 0;

  return isOpen ? (
    <div className="stats__container" onClick={handleClickOutside}>
      <div ref={modalRef} className="stats__content">
        <div className="stats__content__divider">
          <img src="./wordle.svg" alt="wordle logo" height={80} width={90} />
          <h2>Wordle</h2>
          {loading && <Loader />}
          {!auth && !loading ? (
            <p>You need to login to have stats</p>
          ) : (
            auth &&
            !loading &&
            stats && (
              <>
                <h3>Statistics</h3>
                <ul className="stats__player">
                  <li>
                    <span className="stats__value">{stats.gamesPlayed}</span>
                    <span className="stats__name">Played</span>
                  </li>
                  <li>
                    <span className="stats__value">{stats.wordsGuessed}</span>
                    <span className="stats__name">Guessed</span>
                  </li>
                  <li>
                    <span className="stats__value">{winRate}</span>
                    <span className="stats__name">Win %</span>
                  </li>
                  <li>
                    <span className="stats__value">{stats.currentStreak}</span>
                    <span className="stats__name">Current streak</span>
                  </li>
                  <li>
                    <span className="stats__value">{stats.longestStreak}</span>
                    <span className="stats__name">Longest streak</span>
                  </li>
                </ul>
              </>
            )
          )}
          {!auth && (
            <Button
              onClick={handleClickLogin}
              backgroundColor="var(--txt-color-dark)"
              color="var(--main-bg-light)"
            >
              Login
            </Button>
          )}
          <Button
            onClick={onClose}
            backgroundColor="var(--main-color-pink)"
            color="var(--main-bg-light)"
          >
            Quit
          </Button>
        </div>
      </div>
    </div>
  ) : null;
}
