import { createContext, useEffect, useState } from "react";
import axios from "axios";
import { useLocalStorage } from "../hooks/useLocalStorage";
import { useNavigate } from "react-router-dom";
import { googleProvider } from "../utils/firebase";
import { signInWithPopup } from "firebase/auth";
import { auth as firebaseAuth } from "../utils/firebase";

export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [auth, setAuth] = useLocalStorage("auth", null);
  const [errorMessage, setErrorMessage] = useState("");
  const [successMessage, setSuccessMessage] = useState("");
  const [loading, setLoading] = useState(false);
  const [currentStreak, setCurrentStreak] = useState(0);
  const [longestStreak, setLongestStreak] = useState(0);
  const navigate = useNavigate();

  useEffect(() => {
    setErrorMessage("");
    setSuccessMessage("");
    setLoading(false);
  }, [navigate]);

  const startLoading = () => setLoading(true);
  const stopLoading = () => setLoading(false);

  const loginWithPassword = async (email, password) => {
    const emptyFields = [];
    if (!email) emptyFields.push("Email Address");
    if (!password) emptyFields.push("Password");

    if (emptyFields.length > 0) {
      setErrorMessage(
        `Please fill in the following fields: ${emptyFields.join(", ")}`
      );
      return;
    }
    if (password.length < 6) {
      setErrorMessage("Password must be at least 6 characters long.");
      return;
    }
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(email)) {
      setErrorMessage("Please enter a valid email address.");
      return;
    }
    startLoading();
    try {
      const response = await axios.post(
        `${import.meta.env.VITE_API_DOMAIN}/user/login`,
        { email, password },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      if (response.status === 200) {
        setErrorMessage("");
        setAuth({ token: response.data.token, userId: response.data.userId });
        navigate("/");
      }
    } catch (error) {
      console.error(error);
      setErrorMessage(error?.response?.data?.message);
    } finally {
      stopLoading();
    }
  };

  const loginWithGoogle = async () => {
    startLoading();
    try {
      const result = await signInWithPopup(firebaseAuth, googleProvider);
      const user = result.user;
      const response = await axios.post(
        `${import.meta.env.VITE_API_DOMAIN}/user/google`,
        { email: user.email },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      if (response.status === 200) {
        setErrorMessage("");
        setAuth({ token: response.data.token, userId: response.data.userId });
        navigate("/");
      }
    } catch (error) {
      console.error(error);
      setErrorMessage(error?.response?.data?.message);
    } finally {
      stopLoading();
    }
  };

  const registerWithPassword = async (email, password) => {
    const emptyFields = [];
    if (!email) emptyFields.push("Email Address");
    if (!password) emptyFields.push("Password");

    if (emptyFields.length > 0) {
      setErrorMessage(
        `Please fill in the following fields: ${emptyFields.join(", ")}`
      );
      return;
    }

    if (password.length < 6) {
      setErrorMessage("Password must be at least 6 characters long.");
      return;
    }
    
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(email)) {
      setErrorMessage("Please enter a valid email address.");
      return;
    }
    startLoading();
    try {
      const response = await axios.post(
        `${import.meta.env.VITE_API_DOMAIN}/user/register`,
        { email, password },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      if (response.status === 201) {
        setSuccessMessage("User created successfully.");
        setErrorMessage("");
        stopLoading();
        return true;
      }
    } catch (error) {
      setErrorMessage(error?.response?.data?.message);
    } finally {
      stopLoading();
    }
  };

  const logout = () => {
    setAuth(null);
  };

  const getStatsUser = async () => {
    startLoading();
    try {
      const response = await axios.get(
        `${import.meta.env.VITE_API_DOMAIN}/user/stats/${auth.userId}`,
        {
          headers: {
            Authorization: `Bearer ${auth.token}`,
          },
        }
      );
      if (response.status === 200) {
        setCurrentStreak(response.data.stats.currentStreak);
        setLongestStreak(response.data.stats.longestStreak);
        return response.data.stats;
      }
    } catch (error) {
      console.error(error);
    } finally {
      stopLoading();
    }
  };

  const updateStatsUser = async (stats) => {
    startLoading();
    try {
      const response = await axios.put(
        `${import.meta.env.VITE_API_DOMAIN}/user/stats/${auth.userId}`,
        stats,
        {
          headers: {
            Authorization: `Bearer ${auth.token}`,
          },
        }
      );
      if (response.status === 200) {
        return response.data.stats;
      }
    } catch (error) {
      console.error(error);
    } finally {
      stopLoading();
    }
  };

  return (
    <AuthContext.Provider
      value={{
        auth,
        loginWithPassword,
        loginWithGoogle,
        registerWithPassword,
        logout,
        errorMessage,
        successMessage,
        loading,
        getStatsUser,
        updateStatsUser,
        currentStreak,
        setCurrentStreak,
        longestStreak,
        setLongestStreak,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
