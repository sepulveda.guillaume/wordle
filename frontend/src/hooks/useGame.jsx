import { useEffect, useState, useContext } from "react";
import { generate } from "random-words";
import { AuthContext } from "../contexts/AuthContext";

export default function useGame() {
  const [wordToGuess, setWordToGuess] = useState("");
  const [grid, setGrid] = useState(
    Array.from({ length: 6 }, () => Array.from({ length: 5 }, () => null))
  );
  const [nextIndex, setNextIndex] = useState(0);
  const [isWin, setIsWin] = useState(false);
  const [isLose, setIsLose] = useState(false);
  const [checkingRow, setCheckingRow] = useState(null);
  const alphabet = "AZERTYUIOPQSDFGHJKLMWXCVBN".split("");
  const [disabledKeybaord, setDisabledKeyboard] = useState(false);
  const { longestStreak, setLongestStreak, currentStreak, setCurrentStreak } =
    useContext(AuthContext);

  useEffect(() => {
    generateWord();
  }, []);

  const generateWord = () => {
    const words = generate({
      exactly: 1,
      wordsPerString: 1,
      minLength: 5,
      maxLength: 5,
      formatter: (word) => word.toUpperCase(),
    });
    setWordToGuess(words[0]);
  };

  console.log(wordToGuess);

  const handleLetterClick = (letter) => {
    if (disabledKeybaord) return;
    if (nextIndex >= grid.length * grid[0].length || isWin || isLose) {
      return;
    }
    const row = Math.floor(nextIndex / 5);
    const col = nextIndex % 5;

    const newGrid = [...grid];

    newGrid[row][col] = { letter, color: "var(--txt-color-dark)" };

    setGrid(newGrid);
    setNextIndex(nextIndex + 1);

    if ((nextIndex + 1) % 5 === 0) {
      const result = guessWord(row);
      const newGrid = [...grid];
      newGrid[row] = newGrid[row].map((cell, index) => {
        return {
          ...cell,
          color: result[index],
        };
      });
      setGrid(newGrid);
      setCheckingRow(row);
      const isRowCorrect = result.every((color) => color === "good-position");
      if (isRowCorrect) {
        setIsWin(true);
        setCurrentStreak(currentStreak + 1);
        if (currentStreak + 1 > longestStreak) {
          setLongestStreak(currentStreak + 1);
        }
      }
      if (nextIndex + 1 === grid.length * grid[0].length) {
        setIsLose(true);
        setCurrentStreak(0);
      }
    }
  };

  const guessWord = (row) => {
    const guessedWord = grid[row].map((cell) => cell.letter).join("");
    const result = wordToGuess.split("").map((letter, index) => {
      const guessedLetter = guessedWord[index];
      if (guessedLetter === letter) {
        return "good-position";
      } else if (wordToGuess.includes(guessedLetter)) {
        return "bad-position";
      } else {
        return "wrong-letter";
      }
    });
    return result;
  };

  const handleRemoveLetter = () => {
    if (nextIndex <= 0) {
      return;
    }

    const row = Math.floor((nextIndex - 1) / 5);
    const col = (nextIndex - 1) % 5;

    if (col < 0 || col > 3) {
      return;
    }

    const newGrid = [...grid];
    newGrid[row][col] = null;

    setNextIndex(nextIndex - 1);
    setGrid(newGrid);
  };

  const restartGame = () => {
    generateWord();
    setGrid(
      Array.from({ length: 6 }, () => Array.from({ length: 5 }, () => null))
    );
    setNextIndex(0);
    setIsWin(false);
    setIsLose(false);
  };

  return {
    wordToGuess,
    grid,
    alphabet,
    handleLetterClick,
    handleRemoveLetter,
    isWin,
    isLose,
    restartGame,
    checkingRow,
    setCheckingRow,
    guessWord,
    disabledKeybaord,
    setDisabledKeyboard,
    longestStreak,
    currentStreak,
  };
}
