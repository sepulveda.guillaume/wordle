import { Link } from "react-router-dom";

export default function ErrorPage() {
  return (
    <div className="errorpage__container">
      <div className="errorpage__content">
        <p>
          Oops! It looks like you&lsquo;ve reached a page that doesn&rsquo;t
          exist.
        </p>
        <p>
        Click <Link to="/">here</Link> to go back to the home page.
        </p>
      </div>
      <img
        src="https://media.giphy.com/media/14uQ3cOFteDaU/giphy.gif"
        alt="404 error"
      />
    </div>
  );
}
