import { useContext, useEffect, useState } from "react";
import GameBoard from "../components/GameBoard";
import NavBarGame from "../components/NavBarGame";
import useGame from "../hooks/useGame";
import EndGameModal from "../components/EndGameModal";
import StatsModal from "../components/StatsModal";
import { AuthContext } from "../contexts/AuthContext";

export default function GamePage() {
  const [isModalEndGameOpen, setIsModalEndGameOpen] = useState(false);
  const [isModalStatsOpen, setIsModalStatsOpen] = useState(false);
  const [stats, setStats] = useState(null);

  const { auth, getStatsUser, updateStatsUser, loading } =
    useContext(AuthContext);

  const {
    wordToGuess,
    grid,
    alphabet,
    handleLetterClick,
    handleRemoveLetter,
    isWin,
    isLose,
    restartGame,
    checkingRow,
    disabledKeybaord,
    setDisabledKeyboard,
    longestStreak,
    currentStreak,
  } = useGame();

  const updateStats = async () => {
    try {
      const newStats = {
        newGamesPlayed: 1,
        newWordsGuessed: isWin ? 1 : 0,
        newWordsFailed: isLose ? 1 : 0,
        newCurrentStreak: currentStreak,
        newLongestStreak: longestStreak,
      };
      await updateStatsUser({ newStats });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    if (isWin || isLose) {
      if (auth) updateStats();
      setTimeout(() => {
        setIsModalEndGameOpen(true);
      }, 300);
    }
  }, [isWin, isLose]);

  useEffect(() => {
    if (auth) {
      const fetchStats = async () => {
        const stats = await getStatsUser();
        setStats(stats);
      };
      fetchStats();
    }
  }, [auth, isModalStatsOpen]);

  const handleReplayGame = () => {
    setIsModalEndGameOpen(false);
    restartGame();
  };

  return (
    <div className="gamepage__container">
      <NavBarGame setIsModalStatsOpen={setIsModalStatsOpen} />
      <GameBoard
        grid={grid}
        alphabet={alphabet}
        onLetterClick={handleLetterClick}
        onRemoveLetter={handleRemoveLetter}
        checkingRow={checkingRow}
        disabledKeybaord={disabledKeybaord}
        setDisabledKeyboard={setDisabledKeyboard}
      />
      <EndGameModal
        isOpen={isModalEndGameOpen}
        onReplayGame={handleReplayGame}
        isWin={isWin}
        wordToGuess={wordToGuess}
      />
      <StatsModal
        isOpen={isModalStatsOpen}
        onClose={() => setIsModalStatsOpen(false)}
        auth={auth}
        stats={stats}
        loading={loading}
      />
    </div>
  );
}
