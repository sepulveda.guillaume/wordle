import { useNavigate, useOutletContext } from "react-router-dom";
import Button from "../components/Button";
import { AuthContext } from "../contexts/AuthContext";
import { useContext } from "react";
import Footer from "../components/Footer";
import HeaderHome from "../components/HeaderHome";

export default function HomePage() {
  const { auth, logout } = useContext(AuthContext);
  const [, setIsModalRulesOpen] = useOutletContext();

  const navigate = useNavigate();
  return (
    <div className="homepage__container">
      <HeaderHome />
      <div className="homepage__container__buttons">
        <Button onClick={() => setIsModalRulesOpen(true)}>How to play</Button>
        {auth ? (
          <Button
            onClick={logout}
            color="var(--main-bg-light)"
            backgroundColor="var(--main-color-pink)"
          >
            Logout
          </Button>
        ) : (
          <Button
            onClick={() => navigate("/login")}
            color="var(--main-bg-light)"
            backgroundColor="var(--main-color)"
          >
            Login
          </Button>
        )}
        <Button
          onClick={() => navigate("/game")}
          color="var(--main-bg-light)"
          backgroundColor="var(--txt-color-dark)"
        >
          Play
        </Button>
      </div>
      <Footer />
    </div>
  );
}
