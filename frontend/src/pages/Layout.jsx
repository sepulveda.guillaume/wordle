import { Outlet, useLocation } from "react-router-dom";
import RulesModal from "../components/RulesModal";
import { useState } from "react";
import HeaderBar from "../components/HeaderBar";

export default function Layout() {
  const location = useLocation();
  const showHeaderBar =
    location.pathname === "/login" || location.pathname === "/register";

  const [isModalRulesOpen, setIsModalRulesOpen] = useState(false);

  return (
    <div className="layout__container">
      {showHeaderBar && <HeaderBar />}
      <Outlet
        context={[
          isModalRulesOpen,
          setIsModalRulesOpen
        ]}
      />
      <RulesModal
        isOpen={isModalRulesOpen}
        onClose={() => setIsModalRulesOpen(false)}
      />
    </div>
  );
}
