import { useContext, useState } from "react";
import { Icon } from "@iconify/react";
import { Link } from "react-router-dom";
import { AuthContext } from "../contexts/AuthContext";
import Loader from "../components/Loader";

export default function LoginPage() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { loginWithPassword, loginWithGoogle, errorMessage, loading } =
    useContext(AuthContext);

  const handleSubmitForm = async (e) => {
    e.preventDefault();
    const successUserCreated = await loginWithPassword(email, password);
    if (successUserCreated) {
      setEmail("");
      setPassword("");
    }
  };

  const handleGoogleProviderLogin = async () => {
    await loginWithGoogle();
  };

  return (
    <div className="loginpage__container">
      <h2>Login</h2>
      <form onSubmit={handleSubmitForm}>
        <label htmlFor="email">Email</label>
        <input
          type="email"
          id="email"
          name="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
        <label htmlFor="password">Password</label>
        <input
          type="password"
          id="password"
          name="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          required
        />
        <button type="submit">Login</button>
      </form>
      {loading && <Loader />}
      {errorMessage && <p className="error">{errorMessage}</p>}
      <div>
        <p>
          Or By continuing, you agree to the Terms of Sale, Terms of Service,
          and Privacy Policy.
        </p>
        <div className="loginpage__providers">
          <button
            type="submit"
            className="loginpage__providers__btn"
            onClick={handleGoogleProviderLogin}
          >
            <Icon icon="devicon:google" width="1.2em" height="1.2em" />
            <span>Continue with google</span>
          </button>
        </div>
        <div className="loginpage__register">
          <p>
            Don&apos;t have an account? <Link to="/register">Register</Link>
          </p>
        </div>
      </div>
    </div>
  );
}
