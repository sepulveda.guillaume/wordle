import { useContext, useState } from "react";
import { Link } from "react-router-dom";
import { AuthContext } from "../contexts/AuthContext";
import Loader from "../components/Loader";

export default function RegisterPage() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { registerWithPassword, errorMessage, loading, successMessage } =
    useContext(AuthContext);

  const handleSubmitForm = async (e) => {
    e.preventDefault();
    const successUserCreated = await registerWithPassword(email, password);
    if (successUserCreated) {
      setEmail("");
      setPassword("");
    }
  };

  return (
    <div className="loginpage__container">
      {successMessage ? (
        <div className="loginpage__register__successful">
          <p className="success">{successMessage}</p>
          <p className="redirect">
            Now you can <Link to="/login">Login</Link>
          </p>
        </div>
      ) : (
        <>
          <h2>Create your free account</h2>
          <form onSubmit={handleSubmitForm}>
            <label htmlFor="email">Email</label>
            <input
              type="email"
              id="email"
              name="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
            <label htmlFor="password">Password</label>
            <input
              type="password"
              id="password"
              name="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
            <button type="submit">Register</button>
          </form>
          {loading && <Loader />}
          {errorMessage && <p className="error">{errorMessage}</p>}
          <div className="loginpage__register">
            <p>
              By creating an account, you agree to the Terms of Sale, Terms of
              Service, and Privacy Policy.
            </p>
            <p>
              Do you have an account? <Link to="/login">Login</Link>
            </p>
          </div>
        </>
      )}
    </div>
  );
}
