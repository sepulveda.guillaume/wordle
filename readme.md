# Wordle 5 Lettres
Ce projet est une application web permettant aux connecté ou non de résoudre un motus en 5 lettres. Il dispose de 6 chances pour résoudre le casse-tête.

Url de prod pour le front : https://wordle-five-letters.vercel.app

Url de prod pour le back : https://wordle-five-letters-api.vercel.app/

## Fonctionnalités

- Développement du jeu **motus**
- On affiche en vert si la lettre est bien placée, en jaune si elle existe dans le mot mais a la mauvaise place et en rouge si la lettre n'existe pas dans le mot à définer
- Inscription via un email et un password
- Connexion avec email/password
- Inscription / connexion via son compte Google
- Affichage des stats de l'utilisateur et modification après chaque partie (nombre de parties jouées, gagnées, % de victoire, série en cours, série la plus longue pour cet utilisateur).
- Affichage via une modal des règles du jeu.

## Technologies utilisées

- React.js
- Random-words : un paquet npm pour générer les mots de 5 lettres
- Node.js
- Express.js
- MongoDB : Base de données NoSQL pour stocker les utilisateurs
- Firebase : Pour la mise en place du provier Google
- Axios
- JWT (JSON Web Tokens)
- Vercel : déployement du front et back

## Installation

1. Clonez ce dépôt sur votre machine locale :

```bash
git clone https://gitlab.com/sepulveda.guillaume/wordle.git
```

2. Accédez au répertoire du projet :

```bash
cd wordle
```

3. Installez les dépendances pour le frontend, le backend et les dépendances globales :

```bash
cd frontend
npm install

cd ../backend
npm install

cd ..
npm install
```

4. Démarrer le serveur backend et l'application front en même temps à la racine, sinon démarrer les séparément :

```bash
# Démarrer le serveur backend et l'application front en même temps
npm start

# Démarrez le serveur backend :
cd backend
npm run serve

# Démarrez l'application frontend :
cd frontend
npm run dev
```

## Captures

*(à venir)*